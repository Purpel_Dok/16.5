﻿#include <iostream>
#include <time.h>
using namespace std;

int main()
{
setlocale(LC_ALL, "ru");
struct tm buf;
time_t t = time(NULL);
localtime_s(&buf, &t);

const int N = 5;
int calendarday = buf.tm_mday;
// Выводи массив
int array[N][N];
cout << "Массив: \n";
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			cout << array[i][j] << " ";
		}
		cout << endl;
	}
	// Вычисляем индекс строки, согласно колендарному дню
	int index = calendarday % N;

	// Вычксляем сумму элементов выбранной строки
	int sum = 0;
	for (int j = 0; j < N; j++) 
	{
		sum += array[index][j];
	}
	cout << "Сумма элементов в строке с индексом " << index << " равна " << sum << endl;

	return 0;
}




